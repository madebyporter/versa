---
title: Typography
date: 2018-06-25 09:08 UTC
category: design
tags: Graphic Design, Product Design, Web Design
abstract: Typography is the art and technique of arranging type to make written language legible, readable, and appealing when displayed.
links:
  - name: Typography - Wikipedia
    desc: Typography is the art and technique of arranging type to make written language legible, readable, and appealing when displayed.
    type: wiki
    url: https://en.wikipedia.org/wiki/Typography
  - name: Google Fonts
    desc: Making the web more beautiful, fast, and open through great typography.
    type: resource
    url: https://fonts.google.com/
  - name: Typography Cheatsheet
    desc: A comprehensive guide to using proper typographic characters, including correct grammatical usage.
    type: reference
    url: https://www.typewolf.com/cheatsheet
  - name: Type Scale
    desc: A type tool that helps build type scales and output to CSS.
    type: app
    url: http://type-scale.com/
  - name: Practical Typography
    desc: Learn everything about typography.
    type: resource
    url: https://practicaltypography.com/
  - name: Typography Resources
    desc: A curated list of typography articles and resources, categorized.
    type: reference
    url: https://www.whatfontis.com/typography-resources/
  - name: Dribbble - Typography
    desc: The latest typography driven shots from designers on Dribbble.
    type: gallery
    url: https://dribbble.com/search?q=typography
  - name: What Font
    desc: A Google Chrome Extension with which you can easily get font information about the text you are hovering on.
    type: app
    url: http://www.chengyinliu.com/whatfont.html
  - name: Font Squirrel
    desc: Font Squirrel scours the internet for high quality, legitimately free fonts. Download thousands of completely legal, high quality, free fonts.
    type: resource
    url: https://www.fontsquirrel.com/
  - name: I love Typography
    desc: The world's most popular fonts and typography blog
    type: content
    url: https://ilovetypography.com/
  - name: Lost Type Co-op
    desc: Lost Type is a Collaborative Digital Type Foundry
    type: resource
    url: https://losttype.com/
  - name: Typography Handbook
    desc: A concise, referential guide on best web typographic practices.
    type: reference
    url: http://typographyhandbook.com/
  - name: Style Manual
    desc: A reference document by Andy Taylor.
    type: reference
    url: http://stylemanual.org/
  - name: Font Generator
    desc: The Brandmark font generator creates unique font pairings from Google fonts.
    type: tool
    url: http://brandmark.io/font-generator/
---
